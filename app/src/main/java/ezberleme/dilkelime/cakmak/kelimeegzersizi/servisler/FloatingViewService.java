package ezberleme.dilkelime.cakmak.kelimeegzersizi.servisler;

import android.app.Notification;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.PixelFormat;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.IBinder;
import android.speech.tts.TextToSpeech;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.Locale;
import java.util.Random;

import ezberleme.dilkelime.cakmak.kelimeegzersizi.R;
import ezberleme.dilkelime.cakmak.kelimeegzersizi.datebase.Crud;
import ezberleme.dilkelime.cakmak.kelimeegzersizi.datebase.VeriCek;
import ezberleme.dilkelime.cakmak.kelimeegzersizi.datebase.Veritabanim;

public class FloatingViewService extends Service {

    private WindowManager mWindowManager;
    private View mFloatingView;
    Button anlamibtn,anlami1btn,btnKapat;
    String anlami0, kelime, anlami1;
    int kullanilan,id;
    Crud crud;
    Veritabanim veri;
    VeriCek veriCek;
    TextView tv;
    SharedPreferences sharedPreferences;
    TextToSpeech textToSpeech;
    LinearLayout linearLayout;

    public FloatingViewService() {
    }

    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {

        mFloatingView = LayoutInflater.from(this).inflate(R.layout.floating_window, null);
        startForeground(1,new Notification());

        AdView mAdView = (AdView) mFloatingView.findViewById(R.id.adView);
        AdRequest adRequest = new AdRequest.Builder().addTestDevice("5DE009055A7A5F306AB6397C2C4C0375").build();
        mAdView.loadAd(adRequest);
        sharedPreferences= getApplicationContext().getSharedPreferences(getApplicationContext().getPackageName(), Context.MODE_PRIVATE);

        String dbname=sharedPreferences.getString("kullanilandil","default.db");

        //Ekrana, view elementi eklemek için ayar yapıyorz
        final WindowManager.LayoutParams params = new WindowManager.LayoutParams(
                WindowManager.LayoutParams.WRAP_CONTENT,
                WindowManager.LayoutParams.WRAP_CONTENT,
                WindowManager.LayoutParams.TYPE_PHONE,
                WindowManager.LayoutParams.FLAG_NOT_TOUCH_MODAL,
                PixelFormat.TRANSLUCENT);
        params.gravity = Gravity.CLIP_HORIZONTAL;        //Başlangıçta görünüm, sol üst köşeye eklenecek
        params.x = 0;
        params.y = 100;
        //Ekrana, layout icinde barınan view elementi ekliyoruz
        mWindowManager = (WindowManager) getSystemService(WINDOW_SERVICE);
        mWindowManager.addView(mFloatingView, params);

        veri=new Veritabanim(getApplicationContext(),dbname);
        crud=new Crud();
        try {
            veri.createDataBase();
        } catch (IOException e) {
            e.printStackTrace();
        }


        veriCek = crud.tekveriGetir( getApplicationContext());
        anlami0 = veriCek.getAnaDil();
        kelime = veriCek.getHedefDil();
        kullanilan=veriCek.getTekrarSayisi();
        id=veriCek.getId();

        veriCek = crud.tekveriGetir(getApplicationContext());
        anlami1 = veriCek.getAnaDil();

        SharedPreferences preferences = getApplicationContext().getSharedPreferences(getApplicationContext().getPackageName(),MODE_PRIVATE);
        final boolean swicth= preferences.getBoolean("swicth",false);


        Random random = new Random();
        int a = random.nextInt(2);
        tv= mFloatingView.findViewById(R.id.padd);
        tv.setText(kelime);
        anlamibtn = (Button) mFloatingView.findViewById(R.id.btn1);
        anlami1btn = (Button) mFloatingView.findViewById(R.id.btn2);
        btnKapat=mFloatingView.findViewById(R.id.btnkapat);
        linearLayout=mFloatingView.findViewById(R.id.linear);

        btnKapat.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                stopSelf();
            }
        });
        switch (a) {

            case 0:
                anlamibtn.setText(anlami0);
                anlami1btn.setText(anlami1);
                break;
            case 1:
                anlamibtn.setText(anlami1);
                anlami1btn.setText(anlami0);
                break;

        }

        anlamibtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (anlami0.equalsIgnoreCase(anlamibtn.getText().toString())) {
                    Toast.makeText(FloatingViewService.this, "Tebrikler bildiniz", Toast.LENGTH_LONG).show();
                    crud.veriGuncelle(getApplicationContext(), kullanilan + 1, id);
                    if (swicth)
                        companents_init();
                    stopSelf();
                } else {
                    Toast.makeText(FloatingViewService.this, "Yanlış cevap tekrar deneyin", Toast.LENGTH_LONG).show();
                    yanlisCevap();
                }
            }
        });

        anlami1btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (anlami0.equalsIgnoreCase(anlami1btn.getText().toString())) {
                    Toast.makeText(FloatingViewService.this, "Tebrikler bildiniz", Toast.LENGTH_LONG).show();
                    crud.veriGuncelle(getApplicationContext(), kullanilan + 1, id);
                    if (swicth)
                        companents_init();
                    stopSelf();
                } else{
                    Toast.makeText(FloatingViewService.this, "Yanlış cevap tekrar deneyin", Toast.LENGTH_LONG).show();
                    yanlisCevap();
            }
            }
        });

        return START_STICKY;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if (mFloatingView != null) mWindowManager.removeView(mFloatingView);
    }

    private void yanlisCevap(){
        tv.setText(kelime+"\n\n"+anlami0);
        anlami1btn.setVisibility(View.GONE);
        anlamibtn.setVisibility(View.GONE);
        linearLayout.setVisibility(View.VISIBLE);
    }
    private void companents_init()
    {
        //ayalım. ve yapıcı metoda gerekli parametreleri verelim
        // define tts , TextToSpeech Constructor method
        //TextToSpeech(Context context, TextToSpeech.OnInitListener listener)
        textToSpeech = new TextToSpeech(getApplicationContext(), new TextToSpeech.OnInitListener() {
            @Override
            public void onInit(int status)
            {

                //eğerki hata yoksa
                // if there is no error
                if (status != TextToSpeech.ERROR)
                {

                    // belirttiğimiz diller mevcut mu bakalım; bir metod yazalım ve kodlayalım "your_Language()"
                    // eğer belittiğimiz diller dışında da seslendirme yoksa kullanıcıya uyarı verdirelim

                    // see if there are languages ​​we mentioned; Let's write a method and code it "your_Language()"
                    // // let the user warn if there have not voices other than the languages, we give a message

                    int result =  your_Language();

                    if (result == TextToSpeech.LANG_MISSING_DATA || result == TextToSpeech.LANG_NOT_SUPPORTED)
                    {


                    }
                    else
                    {

                        //run TTS
                        // şimdi konuşmaya başla ahbab :)
                        // come on dude! start talking to me now :)
                        speakOut();
                    }
                }
                else
                {

                }
            }
        });

    }

    // bir metod yazalım. burda tts sınıfının speak metodunu kullanacağız
    // Write a method. Here we will use the tts class's speak method
    // speak(String text, int queueMode, HashMap<String, String> params)
    private void speakOut()
    {

        // ilk olarak edittext içerisindeki veriyi alalım
        // first get the data  from edittext

        //söylediğimiz metodu çağırın
        //Call the method we say
        textToSpeech.speak(anlami0.toLowerCase(), TextToSpeech.QUEUE_FLUSH, null);

    }


    // otomatik sesledirme dillerini seçme metodu
    // Method of selecting automatic voicing languages
    boolean isSupported;

    private int your_Language()
    {

        isSupported = false;

        String localLanguage = Locale.getDefault().toString();

//        1	US
//        2	CANADA_FRENCH
//        3	GERMANY
//        4	ITALY
//        5	JAPAN
//        6	CHINA

        // türkçe dili var olanlar içerisinde gelmediğinden bunu şu şekilde ayarlayabiliriz
        // eğer ki dil seçimi içeren uygulamalar için doğru telaffuzu bu şekilde sağlarız
        // bunun testini yapabilirsiniz. ayarlar kısmından telefon dilini başka bir dil ile değiştirin deneyin ve bir de değiştirmeden deneyin

        // My dialing language is Turkish, so it is not in the default languages, so I dont need to change the slice so that pronunciation is correct,
        // You can do it this way if this applies to you
        // If we have the right pronunciation for the applications that contain the language selection. its wonderful
        // you can test it. Try changing the phone language to another language in settings and try again without changing


        if (!Locale.getDefault().toString().equals("tr_TR"))
        {
            if (localLanguage.equals(Locale.FRANCE.toString()))
            {
                textToSpeech.setLanguage(Locale.FRANCE);

            }
            else if (localLanguage.equals(Locale.FRENCH.toString()))
            {
                textToSpeech.setLanguage(Locale.FRENCH);
                isSupported = true;

            }
            else if (localLanguage.equals(Locale.FRENCH.toString()))
            {
                textToSpeech.setLanguage(Locale.FRENCH);
                isSupported = true;

            }
            else if (localLanguage.equals(Locale.ITALIAN.toString()))
            {
                textToSpeech.setLanguage(Locale.ITALIAN);
                isSupported = true;

            }
            else if (localLanguage.equals(Locale.ITALY.toString()))
            {
                textToSpeech.setLanguage(Locale.ITALY);
                isSupported = true;

            }
            else if (localLanguage.equals(Locale.US.toString()))
            {
                textToSpeech.setLanguage(Locale.US);
                isSupported = true;

            }
            else if (localLanguage.equals(Locale.UK.toString()))
            {
                textToSpeech.setLanguage(Locale.UK);
                isSupported = true;

            }
            else if (localLanguage.equals(Locale.ENGLISH.toString()))
            {
                textToSpeech.setLanguage(Locale.ENGLISH);
            }
            else
            {
                isSupported = false;
            }

        }
        else
        {
            isSupported = true;
        }


        int  value;
        return value = (isSupported) ? 1 : -1;

    }

}