package ezberleme.dilkelime.cakmak.kelimeegzersizi;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.animation.AnimationUtils;
import android.view.animation.LayoutAnimationController;
import android.widget.Toast;

import java.util.ArrayList;

import ezberleme.dilkelime.cakmak.kelimeegzersizi.adapter.CardViewAdapter;
import ezberleme.dilkelime.cakmak.kelimeegzersizi.adapter.MyAdapter;
import ezberleme.dilkelime.cakmak.kelimeegzersizi.adapter.Recmodel;
import ezberleme.dilkelime.cakmak.kelimeegzersizi.adapter.RecyclerViewItemClickListener;
import ezberleme.dilkelime.cakmak.kelimeegzersizi.adapter.Viewpageradapteriki;
import ezberleme.dilkelime.cakmak.kelimeegzersizi.datebase.Crud;
import ezberleme.dilkelime.cakmak.kelimeegzersizi.datebase.VeriCek;
import ezberleme.dilkelime.cakmak.kelimeegzersizi.fragmentler.FragmentAyalar;
import ezberleme.dilkelime.cakmak.kelimeegzersizi.fragmentler.FragmentBasari;
import ezberleme.dilkelime.cakmak.kelimeegzersizi.fragmentler.FragmentOyun;
import ezberleme.dilkelime.cakmak.kelimeegzersizi.servisler.Receiver;

public class MainActivity extends AppCompatActivity {


    private static final int CODE_DRAW_OVER_OTHER_APP_PERMISSION = 2084;
    public static ViewPager viewPager;
    private TabLayout tabLayout;
    SharedPreferences sharedPreferences;
    Toolbar toolbar;
    int selectedPosition=-1;
    Crud crud;
    int resimler[]={R.drawable.insanlar,R.drawable.gorunum,R.drawable.saglik,R.drawable.ev,
            R.drawable.hizmetler,R.drawable.alisveris,R.drawable.yiyecekler,R.drawable.fesfood,R.drawable.egitim,
            R.drawable.ishayati,R.drawable.ulasim,R.drawable.spor,R.drawable.boszamanlar,R.drawable.dogalcevre,
            R.drawable.bilgilenme};

    // Test iconlari
    private int[] tabIcons = {android.R.drawable.ic_menu_camera,android.R.drawable.ic_menu_agenda,
            android.R.drawable.ic_menu_add,android.R.drawable.ic_dialog_dialer};
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        /*sharedPreferences= getSharedPreferences(getPackageName(),MODE_PRIVATE);
        SharedPreferences.Editor  editor=sharedPreferences.edit();
        editor.putBoolean("ezberleme.dilkelime.cakmak.kelimeegzersizi",false);

       int saataraligi=sharedPreferences.getInt("saataraligi",29);

       if (saataraligi<=29){
            editor.putInt("saataraligi",30);
            editor.putInt("progresvarsayılan",3);
            editor.apply();
        }*/

/*
        toolbar=findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("Merhaba Aga");*/



        String isimler[]={"İnsanlar","Dış Görünüm","Sağlık","Ev","Hizmetler","Alışveriş","Yiyecekler","FasFood",
                "Eğetim-Öğretim","İş hayatı","Ulaşım","Spor","Boş Zamanlar","Doğal Çevre","Bilgilenme"};
        final ArrayList<Recmodel> rc=new ArrayList<Recmodel>();
        ArrayList<VeriCek> veriCeks=new ArrayList<>();
        crud=new Crud();
        veriCeks=crud.recVericek(getApplicationContext(),"default.db",0);
        for (int i=0;i<=resimler.length-1;i++){

            Recmodel rec=new Recmodel();
            rec.setIsim(veriCeks.get(i).getAnaDil());
            rec.setResim(resimler[i]);
            rc.add(rec);

        }


        final MyAdapter myAdapter=new MyAdapter(rc);
        RecyclerView listView=findViewById(R.id.resc);

        listView.setAdapter(myAdapter);
        LinearLayoutManager linearLayoutManager=new LinearLayoutManager(getApplicationContext());
        linearLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        listView.setLayoutManager(linearLayoutManager);

        final RecyclerView recyclerView=findViewById(R.id.rec);

        CardViewAdapter cardadabter=new CardViewAdapter(rc,getApplicationContext());

        LinearLayoutManager cardlinear=new LinearLayoutManager(getApplicationContext());
        cardlinear.setOrientation(LinearLayoutManager.VERTICAL);
        recyclerView.setLayoutManager(cardlinear);


        Viewpageradapteriki viewpageradapteriki=new Viewpageradapteriki(getApplicationContext(),rc);



        cardadabter.setOnItemClickListener(new RecyclerViewItemClickListener() {
            @Override
            public void onItemClick(View view, int position) {
                Toast.makeText(MainActivity.this, "oldu bu iş "+position, Toast.LENGTH_SHORT).show();

                FragmentOyun fragmentOyun=new FragmentOyun();
                android.support.v4.app.FragmentManager manager=getSupportFragmentManager();
                android.support.v4.app.FragmentTransaction transaction=manager.beginTransaction();
                transaction.add(R.id.main,fragmentOyun,"Ayarlar");
                transaction.addToBackStack(null);
                transaction.commit();
            }

            @Override
            public void onItemLongClick(View view, int position) {

            }
        });

        recyclerView.setAdapter(cardadabter);


        myAdapter.setOnItemClickListener(new RecyclerViewItemClickListener() {
           @Override
           public void onItemClick(View view, int position) {
               Toast.makeText(MainActivity.this,"Deneme; "+position,Toast.LENGTH_LONG).show();
               ArrayList<VeriCek> veri=new ArrayList<>();
               final ArrayList<Recmodel> rcc=new ArrayList<Recmodel>();

               veri=crud.recVericek(getApplicationContext(),"default.db",8823);

               for (int i=0;i<=resimler.length-1;i++){

                   Recmodel reccc=new Recmodel();
                   reccc.setIsim(veri.get(i).getAnaDil());
                   reccc.setResim(resimler[i]);
                   rcc.add(reccc);

               }


              runAnimation(recyclerView,rcc);


               myAdapter.notifyDataSetChanged();


           }

           @Override
           public void onItemLongClick(View view, int position) {

           }
       });


        Intent i=new Intent(MainActivity.this,Receiver.class);
        PendingIntent pendingIntent= PendingIntent.getBroadcast(MainActivity.this,12,i,0);
        AlarmManager alarmManager= (AlarmManager) getSystemService(ALARM_SERVICE);

        /*viewPager = (ViewPager) findViewById(R.id.viewpager);
        setupViewPager(viewPager);
        final ViewPagerAdapter adapter= setupViewPager(viewPager);

        tabLayout = (TabLayout) findViewById(R.id.tabs);
        tabLayout.setupWithViewPager(viewPager);
        tabLayout.getTabAt(0).setIcon(tabIcons[0]);
        *//*tabLayout.getTabAt(1).setIcon(tabIcons[1]);
        tabLayout.getTabAt(2).setIcon(tabIcons[2]);
        tabLayout.getTabAt(3).setIcon(tabIcons[3]);*//*
*/

        /*if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M && !Settings.canDrawOverlays(this)) {

            Intent intent = new Intent(Settings.ACTION_MANAGE_OVERLAY_PERMISSION,
                    Uri.parse("package:" + getPackageName()));
            startActivityForResult(intent, CODE_DRAW_OVER_OTHER_APP_PERMISSION);
        }else {
            if (alarmManager != null) {
                alarmManager.setInexactRepeating(AlarmManager.ELAPSED_REALTIME, 60 * 1500,
                        saataraligi*60 * 1000, pendingIntent);
            }

        }*/


    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == CODE_DRAW_OVER_OTHER_APP_PERMISSION) {
            //İzin verilme kontrolu
            if (resultCode == RESULT_OK) {
            } else { //İzin verilmediği durum
                Toast.makeText(this,
                        "Uygulamayı kullanabilmek için lütfen izin veriniz",
                        Toast.LENGTH_SHORT).show();
                finish();
            }
        } else {
            super.onActivityResult(requestCode, resultCode, data);
        }
    }

    /*private ViewPagerAdapter setupViewPager(ViewPager viewPager) {
        ViewPagerAdapter adapter = new ViewPagerAdapter(getSupportFragmentManager());
       // adapter.addFragment(new FragmentAyalar(),getResources().getString(R.string.tab_ayarlar));
      //  adapter.addFragment(new FragmentBasari(),getResources().getString(R.string.tab_basari));
       // adapter.addFragment(new FragmentKelimeListesi(),getResources().getString(R.string.tab_kelimeler));
        adapter.addFragment(new FragmentOyun(),"Ana Menü");
        viewPager.setAdapter(adapter);
        return adapter;
    }
*/


    public void runAnimation(RecyclerView recyclerView,ArrayList<Recmodel> rec){

        Context context=recyclerView.getContext();
        LayoutAnimationController controller=null;

        controller=AnimationUtils.loadLayoutAnimation(context,R.anim.layouth_slide_from);
        recyclerView.setAdapter(new CardViewAdapter(rec,MainActivity.this));


        recyclerView.setLayoutAnimation(controller);
        recyclerView.getAdapter().notifyDataSetChanged();
        recyclerView.scheduleLayoutAnimation();

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main_menu,menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        int id=item.getItemId();
        android.support.v4.app.FragmentManager manager=getSupportFragmentManager();
        android.support.v4.app.FragmentTransaction transaction=manager.beginTransaction();
        switch (id){
            case R.id.menu1:

                FragmentAyalar fragmentAyalar=new FragmentAyalar();


                transaction.add(R.id.main,fragmentAyalar,"Ayarlar");
                transaction.addToBackStack(null);
                transaction.commit();
                break;

            case R.id.menu2:
                FragmentBasari fragmentBasari=new FragmentBasari();


                transaction.add(R.id.main,fragmentBasari,"Başarı");
                transaction.addToBackStack(null);

                transaction.commit();
                break;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        if (getFragmentManager().getBackStackEntryCount() > 1) {
            getFragmentManager().popBackStack();
        } else {
            super.onBackPressed();
        }
    }
}
