package ezberleme.dilkelime.cakmak.kelimeegzersizi.adapter;

public class Recmodel {

    String isim;
    int Resim;
    int medyaid;

    public int getMedyaid() {
        return medyaid;
    }

    public void setMedyaid(int medyaid) {
        this.medyaid = medyaid;
    }

    public String getIsim() {
        return isim;
    }

    public void setIsim(String isim) {
        this.isim = isim;
    }

    public int getResim() {
        return Resim;
    }

    public void setResim(int resim) {
        Resim = resim;
    }
}
