package ezberleme.dilkelime.cakmak.kelimeegzersizi.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.ArrayList;

import ezberleme.dilkelime.cakmak.kelimeegzersizi.R;
import ezberleme.dilkelime.cakmak.kelimeegzersizi.datebase.Downloaddatabase;

/**
 * Created by cakmmak on 13.04.2018.
 */

public class DownloaddataBaseAdapter extends ArrayAdapter{


    int teksatir,textview;
    ArrayList<Downloaddatabase> veriler;
    Context context;
    View itemView;

    public DownloaddataBaseAdapter(@NonNull Context context, int teksatir, int textview, ArrayList<Downloaddatabase> veriler) {
        super(context,teksatir,veriler);
        this.context=context;
        this.teksatir=teksatir;
        this.textview=textview;
        this.veriler=veriler;
    }



    public View getView(final int position, View convertView, final ViewGroup parent) {


        itemView=convertView;
        if (itemView==null) {
            LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            itemView = inflater.inflate(teksatir, parent, false);

        }

        final TextView ogrenilenlayauthanlami = (TextView) itemView.findViewById(R.id.download_list_tv);
        ogrenilenlayauthanlami.setText(veriler.get(position).getAdi());


        return itemView;

    }

}
