package ezberleme.dilkelime.cakmak.kelimeegzersizi.adapter;

import android.content.Context;
import android.content.SharedPreferences;
import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

import ezberleme.dilkelime.cakmak.kelimeegzersizi.R;
import ezberleme.dilkelime.cakmak.kelimeegzersizi.datebase.VeriCek;

/**
 * Created by cakmmak on 15.03.2018.
 */

public class OgrenilenArrayAdapter extends ArrayAdapter {

    int teksatir,textview;
    ArrayList<VeriCek> veriler;
    Context context;
    View itemView;

    public OgrenilenArrayAdapter(@NonNull Context context,int teksatir,int textview,ArrayList<VeriCek> veriler) {
        super(context,teksatir,veriler);
        this.context=context;
        this.teksatir=teksatir;
        this.textview=textview;
        this.veriler=veriler;
    }



    public View getView(final int position, View convertView, final ViewGroup parent) {
        SharedPreferences sharedPrefs = context.getSharedPreferences(context.getPackageName(), Context.MODE_PRIVATE);
        final SharedPreferences.Editor editor = sharedPrefs.edit();

        itemView=convertView;
        if (itemView==null) {
            LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            itemView = inflater.inflate(teksatir, parent, false);

        }



        final TextView ogrenilenlayauthanlami = (TextView) itemView.findViewById(R.id.ogrenilen_layauth_anlami);
        TextView ogrenilenlayauthkelime = (TextView) itemView.findViewById(R.id.ogrenilen_layauth_kelime);
        TextView ogrenilenlayauthkullanilan = (TextView) itemView.findViewById(R.id.ogrenilen_layauth_kullanilan);

        ogrenilenlayauthanlami.setText(veriler.get(position).getAnaDil());
        ogrenilenlayauthkelime.setText(veriler.get(position).getHedefDil());
        ogrenilenlayauthkullanilan.setText(String.valueOf(veriler.get(position).getTekrarSayisi()+" defa tekrarlandı."));


        return itemView;

    }


}
