package ezberleme.dilkelime.cakmak.kelimeegzersizi.datebase;

import android.content.ContentValues;
import android.content.Context;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import java.io.IOException;
import java.util.ArrayList;

/**
 * Created by cakmmak on 22.02.2018.
 */

public class Crud {


    Veritabanim veritabanim;

    VeriCek veriCek;
    SharedPreferences sharedPreferences;
    SharedPreferences.Editor editor;
    Downloaddatabase downloaddatabase;
    String tablename="language";

    //Database üzerinde sorgu yurutur
    public ArrayList<VeriCek> sorguYurut(Context context, String tekrarSayisi, String islem)
    {
        ArrayList<VeriCek> veri=new ArrayList<>();
        sharedPreferences= context.getSharedPreferences(context.getPackageName(),Context.MODE_PRIVATE);

        String dbname=sharedPreferences.getString("kullanilandil", "default.db");
        try {
            veritabanim=new Veritabanim(context,dbname);
            veritabanim.createDataBase();
        } catch (IOException e) {
            e.printStackTrace();
        }
        SQLiteDatabase myDb=veritabanim.getWritableDatabase();

        Cursor cursor;
        cursor = myDb.rawQuery("select * from "+tablename+" where tekrarsayisi "+ islem + tekrarSayisi+" AND tekrarsayisi >0",
                null);
        while (cursor.moveToNext()){
            String anaDil = cursor.getString(cursor.getColumnIndex("anadil"));
            String hedefDil = cursor.getString(cursor.getColumnIndex("hedefdil"));
            int tekrarSayisiSorgu = cursor.getInt(cursor.getColumnIndex("tekrarsayisi"));
            int id=cursor.getInt(cursor.getColumnIndex("id"));
            veriCek=new VeriCek();
            veriCek.setId(id);
            veriCek.setAnaDil(anaDil);
            veriCek.setHedefDil(hedefDil);
            veriCek.setTekrarSayisi(tekrarSayisiSorgu);
            veri.add(veriCek);
        }
        myDb.close();
        cursor.close();
        return veri;

    }

    //Toplu liste
    public ArrayList<VeriCek> topluListe(Context context)
    {
        ArrayList<VeriCek> veri=new ArrayList<>();
        sharedPreferences= context.getSharedPreferences(context.getPackageName(),Context.MODE_PRIVATE);
        String dbname=sharedPreferences.getString("kullanilandil", "default.db");

        try {
            veritabanim=new Veritabanim(context,dbname);
            veritabanim.createDataBase();
        } catch (IOException e) {
            e.printStackTrace();
        }
        SQLiteDatabase myDb=veritabanim.getWritableDatabase();
        Cursor cursor;
        cursor = myDb.rawQuery("select * from "+tablename, null);

        while (cursor.moveToNext()){
            String anaDil = cursor.getString(cursor.getColumnIndex("anadil"));
            String hedefDil = cursor.getString(cursor.getColumnIndex("hedefdil"));
            int tekrarSayisiSorgu = cursor.getInt(cursor.getColumnIndex("tekrarsayisi"));
            veriCek=new VeriCek();
            veriCek.setAnaDil(anaDil);
            veriCek.setHedefDil(hedefDil);
            veriCek.setTekrarSayisi(tekrarSayisiSorgu);
            veri.add(veriCek);
        }

        myDb.close();
        cursor.close();

        return veri;

    }

    //veri ekler
    public  void veriEkle(Context context,ContentValues values)
    {
        sharedPreferences= context.getSharedPreferences(context.getPackageName(),Context.MODE_PRIVATE);
        String dbname=sharedPreferences.getString("kullanilandil", "default.db");

        try {
            veritabanim=new Veritabanim(context,dbname);
            veritabanim.createDataBase();
        } catch (IOException e) {
            e.printStackTrace();
        }
        SQLiteDatabase myDb=veritabanim.getWritableDatabase();
        myDb.insert(tablename,null,values);
        myDb.close();

    }

    //database download
    public ArrayList<Downloaddatabase> databaseDownload(Context context,String langCode)
    {
        ArrayList<Downloaddatabase> veri=new ArrayList<>();

        try {
            veritabanim=new Veritabanim(context, "default.db");
            veritabanim.createDataBase();
        } catch (IOException e) {
            e.printStackTrace();
        }
        SQLiteDatabase myDb=veritabanim.getWritableDatabase();
        Cursor cursor;
        cursor = myDb.rawQuery("select * from download where langcode='"+langCode+"'", null);

        while (cursor.moveToNext()){
            String adi = cursor.getString(cursor.getColumnIndex("adi"));
            String databaselink = cursor.getString(cursor.getColumnIndex("databaselink"));
            String dbismi = cursor.getString(cursor.getColumnIndex("dbadi"));
            downloaddatabase=new Downloaddatabase();
            downloaddatabase.setAdi(adi);
            downloaddatabase.setLink(databaselink);
            downloaddatabase.setDbadi(dbismi);
            veri.add(downloaddatabase);
        }

        myDb.close();
        cursor.close();

        return veri;

    }
    //veri güncelle
    public  void veriGuncelle(Context context,int values,int guncellenecekId)
    {
        sharedPreferences= context.getSharedPreferences(context.getPackageName(),Context.MODE_PRIVATE);
        String dbname=sharedPreferences.getString("kullanilandil", "default.db");

        try {
            veritabanim=new Veritabanim(context,dbname);
            veritabanim.createDataBase();
        } catch (IOException e) {
            e.printStackTrace();
        }

        ContentValues cv=new ContentValues();

        cv.put("tekrarsayisi",values);
        SQLiteDatabase myDb=veritabanim.getWritableDatabase();
        myDb.update(tablename, cv, "id=" + guncellenecekId, null);
        myDb.close();

    }

    //Tekbir veri getir
    public VeriCek tekveriGetir(Context context) {
        sharedPreferences= context.getSharedPreferences(context.getPackageName(),Context.MODE_PRIVATE);
        String dbname=sharedPreferences.getString("kullanilandil", "default.db");

        try {
            veritabanim=new Veritabanim(context,dbname);
            veritabanim.createDataBase();
        } catch (IOException e) {
            e.printStackTrace();
        }
        SQLiteDatabase SQLiteDatabaseInstance_=veritabanim.getWritableDatabase();
        sharedPreferences=context.getSharedPreferences(context.getPackageName(),Context.MODE_PRIVATE);
        int tekrarSayisi=sharedPreferences.getInt("tekrarsayisi",5);

        Cursor cursor = null;
        String hedefDil = "";
        String anadil = "";
        int id1 ;
        try{

            cursor = SQLiteDatabaseInstance_.rawQuery("SELECT * FROM (SELECT * FROM "+tablename+" where tekrarsayisi" +
                            " <= "+tekrarSayisi+" LIMIT 10) order by RANDOM() LIMIT 1",
                    null);

            if(cursor.getCount() > 0) {

                cursor.moveToFirst();
                hedefDil = cursor.getString(cursor.getColumnIndex("hedefdil"));
                anadil = cursor.getString(cursor.getColumnIndex("anadil"));
                id1 = cursor.getInt(cursor.getColumnIndex("id"));
                int kullanilan = cursor.getInt(cursor.getColumnIndex("tekrarsayisi"));
                veriCek=new VeriCek(anadil, hedefDil,id1,kullanilan);
            }
            return veriCek;
        }finally {

            cursor.close();
        }
    }



    //Toplu liste
    public ArrayList<VeriCek> recVericek(Context context,String dbname,int id)
    {
        ArrayList<VeriCek> veri=new ArrayList<>();

        try {
            veritabanim=new Veritabanim(context,dbname);
            veritabanim.createDataBase();
        } catch (IOException e) {
            e.printStackTrace();
        }
        SQLiteDatabase myDb=veritabanim.getWritableDatabase();
        Cursor cursor;
        cursor = myDb.rawQuery("select * from langus where baseid="+id, null);

        while (cursor.moveToNext()){
            String anaDil = cursor.getString(cursor.getColumnIndex("tr"));
            String hedefDil = cursor.getString(cursor.getColumnIndex("en"));
            int tekrarSayisiSorgu = cursor.getInt(cursor.getColumnIndex("baseid"));
            veriCek=new VeriCek();
            veriCek.setAnaDil(anaDil);
            veriCek.setHedefDil(hedefDil);
            veriCek.setTekrarSayisi(tekrarSayisiSorgu);
            veri.add(veriCek);
        }

        myDb.close();
        cursor.close();

        return veri;

    }

}
