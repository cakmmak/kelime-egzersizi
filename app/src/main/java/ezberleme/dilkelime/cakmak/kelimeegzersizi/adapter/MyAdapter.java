package ezberleme.dilkelime.cakmak.kelimeegzersizi.adapter;

import android.graphics.Color;
import android.support.constraint.ConstraintLayout;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;

import ezberleme.dilkelime.cakmak.kelimeegzersizi.R;

public class MyAdapter extends RecyclerView.Adapter<MyAdapter.MyViewHolder> {

    ArrayList<Recmodel> rc;

    int selectedPosition=-1;
    private RecyclerViewItemClickListener recyclerViewItemClickListener;


    public MyAdapter(ArrayList<Recmodel> rcData) {
        this.rc=rcData;
    }

    @Override
    public MyViewHolder onCreateViewHolder(final ViewGroup parent, int viewType) {
        // Card_view layoutmuzu kullanarak yeni bir view inflate ettik
        final View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.recycler_view, parent, false);
        // Olusturdugumuz ViewHolder classına az önce olusturdugumuz view'ı verdik
        final MyViewHolder vh = new MyViewHolder(v);

        v.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                recyclerViewItemClickListener.onItemClick(v,vh.getAdapterPosition());
            }
        });


        return vh;
    }

    //item click e dokunma özelliği için kullandığımız metot
    public void setOnItemClickListener(RecyclerViewItemClickListener recyclerViewItemClickListener) {
        this.recyclerViewItemClickListener = recyclerViewItemClickListener;
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, final int position) {

        Picasso.get().load(rc.get(position).Resim).fit().into(holder.image);
        holder.tv.setText(rc.get(position).isim);
        holder.tv.setSelected(true);


    }



    @Override
    public int getItemCount() {
        return rc.size();
    }







    class MyViewHolder extends RecyclerView.ViewHolder{

        ImageView image;
        TextView tv;
        public MyViewHolder(final View itemView) {
            super(itemView);
            tv=itemView.findViewById(R.id.textView2);
            image=itemView.findViewById(R.id.imageView7);



        }
    }

}
