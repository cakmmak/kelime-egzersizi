package ezberleme.dilkelime.cakmak.kelimeegzersizi.fragmentler;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.support.constraint.ConstraintLayout;
import android.support.v4.app.Fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;

import java.io.File;
import java.util.ArrayList;
import java.util.Collections;

import ezberleme.dilkelime.cakmak.kelimeegzersizi.R;
import ezberleme.dilkelime.cakmak.kelimeegzersizi.adapter.OgrenilenArrayAdapter;
import ezberleme.dilkelime.cakmak.kelimeegzersizi.adapter.OgrenilenKelimelerArrayAdapter;
import ezberleme.dilkelime.cakmak.kelimeegzersizi.datebase.Crud;
import ezberleme.dilkelime.cakmak.kelimeegzersizi.datebase.VeriCek;
import ezberleme.dilkelime.cakmak.kelimeegzersizi.datebase.Veritabanim;

public class FragmentBasari extends Fragment  {

    TextView toplamKelimeTv2,ogrenilenkelimetv2,kullanılankelimepakedi_tv2,tekrarlanacakkelimetv2;
    Veritabanim veritabanim;
    Crud crud;
    SharedPreferences sharedPreferences;
    SharedPreferences.Editor editor;
    ConstraintLayout tekrarlanacakContsrain,ogrenilenConstrain,toplamKelimeContrain,kullanilankelimepakedi;
    ListView listView;
    int tekrarSayisi;
    View v;

    public FragmentBasari() {
    }
    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        v= inflater.inflate(R.layout.fragment_basari, container, false);

        init();
        datacekme();

        tekrarlanacakContsrain.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //MainActivity.viewPager.setCurrentItem(2);
                tekrarlanacakKelimelerMetod();
            }
        });

        kullanilankelimepakedi.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                datebaselistesi();
            }
        });

        ogrenilenConstrain.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //MainActivity.viewPager.setCurrentItem(2);
                ogrenilenKelimelerMetod();
            }
        });
        toplamKelimeContrain.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //MainActivity.viewPager.setCurrentItem(2);
                toplamKelimeMetod();
            }
        });

        return v;
    }
    public void tekrarlanacakKelimelerMetod() {
        crud = new Crud();
        final View view = (this).getLayoutInflater().inflate(R.layout.dialog, null);
        final ListView list = (ListView) view.findViewById(R.id.list);
        sharedPreferences = getActivity().getSharedPreferences(getActivity().getPackageName(), Context.MODE_PRIVATE);
        int tekrarSayisi = sharedPreferences.getInt("tekrarsayisi", 5);

        ArrayList<VeriCek> tekrarlanacakKelimeler = (ArrayList<VeriCek>) crud.sorguYurut(getActivity(),
                String.valueOf(tekrarSayisi), "<");
        OgrenilenArrayAdapter adapter = new OgrenilenArrayAdapter(getActivity(), R.layout.ogrenilen_layouth, R.id.ogrenilen_layauth_kelime, tekrarlanacakKelimeler);
        list.setAdapter(adapter);
        final AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setTitle(getResources().getString(R.string.tekrarlanacak_kelimer));
        builder.setCancelable(true);
        builder.setView(view);
        builder.show();

    }

    public void ogrenilenKelimelerMetod() {
        crud = new Crud();
        final View view = (this).getLayoutInflater().inflate(R.layout.dialog, null);
        listView = (ListView) view.findViewById(R.id.list);
        final ArrayList<VeriCek> tekrarlanacakKelimeler = (ArrayList<VeriCek>) crud.sorguYurut(getActivity(),
                String.valueOf(tekrarSayisi), ">=");

        final OgrenilenKelimelerArrayAdapter adapter = new OgrenilenKelimelerArrayAdapter(getActivity(),R.layout.ogrenilen_kelimeler_layouth,R.id.ogrenilen_kelimeler_layauth_anlami,tekrarlanacakKelimeler);
        listView.setAdapter(adapter);
        final AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setTitle(getResources().getString(R.string.ogrenilen_kelime));
        builder.setCancelable(true);
        builder.setPositiveButton("Tamam", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                datacekme();
            }
        });
        builder.setView(view);
        builder.show();

    }
    public void toplamKelimeMetod() {
        crud = new Crud();

        final View view = (this).getLayoutInflater().inflate(R.layout.dialog, null);

        final ListView list = (ListView) view.findViewById(R.id.list);

        final ArrayList<VeriCek> toplamKelime = (ArrayList<VeriCek>) crud.topluListe(getActivity());

        OgrenilenArrayAdapter adapter = new OgrenilenArrayAdapter(getActivity(), R.layout.ogrenilen_layouth, R.id.ogrenilen_layauth_kelime, toplamKelime);
        list.setAdapter(adapter);

        final AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setTitle(getResources().getString(R.string.toplam_kelime));
        builder.setCancelable(true);

        builder.setView(view);
        builder.show();

    }

    public void datebaselistesi(){

        sharedPreferences=getActivity().getSharedPreferences(getActivity().getPackageName(),Context.MODE_PRIVATE);
        editor=sharedPreferences.edit();
        final ArrayList<String> deneme=new ArrayList<String>();
        final ArrayList<String> dbadi=new ArrayList<String>();
        final View view = (this).getLayoutInflater().inflate(R.layout.dialog, null);
        final ListView list = (ListView) view.findViewById(R.id.list);
        String storagePath= "/data/data/" + getActivity().getPackageName() + "/databases/";

        File directory = new File(storagePath);
        File[] files = directory.listFiles();

        for (int i = 0; i < files.length; i++)
        {
            if (files[i].getName().contains("-")&&!files[i].getName().contains("journal")) {
                dbadi.add(files[i].getName());
                if (files[i].getName().contains("-ar-1"))
                    deneme.add(getResources().getString(R.string.ogrenilen_dil_arapca)+" 1");
                else if (files[i].getName().contains("-ar-2"))
                    deneme.add(getResources().getString(R.string.ogrenilen_dil_arapca)+" 2");
                else if (files[i].getName().contains("-de-1"))
                    deneme.add(getResources().getString(R.string.ogrenilen_dil_almanca)+" 1");
                else if (files[i].getName().contains("-de-2"))
                    deneme.add(getResources().getString(R.string.ogrenilen_dil_almanca)+" 2");
                else if (files[i].getName().contains("-zh-1"))
                    deneme.add(getResources().getString(R.string.ogrenilen_dil_cince)+" 1");
                else if (files[i].getName().contains("-zh-2"))
                    deneme.add(getResources().getString(R.string.ogrenilen_dil_cince)+" 2");
                else if (files[i].getName().contains("-hi-1"))
                    deneme.add(getResources().getString(R.string.ogrenilen_dil_hintce)+" 1");
                else if (files[i].getName().contains("-hi-2"))
                    deneme.add(getResources().getString(R.string.ogrenilen_dil_hintce)+" 2");
                else if (files[i].getName().contains("-fr-1"))
                    deneme.add(getResources().getString(R.string.ogrenilen_dil_fransızca)+" 1");
                else if (files[i].getName().contains("-fr-2"))
                    deneme.add(getResources().getString(R.string.ogrenilen_dil_fransızca)+" 2");
                else if (files[i].getName().contains("-ja-1"))
                    deneme.add(getResources().getString(R.string.ogrenilen_dil_japonca)+" 1");
                else if (files[i].getName().contains("-ja-2"))
                    deneme.add(getResources().getString(R.string.ogrenilen_dil_japonca)+" 2");
                else if (files[i].getName().contains("-es-1"))
                    deneme.add(getResources().getString(R.string.ogrenilen_dil_ispanyolca)+" 1");
                else if (files[i].getName().contains("-es-2"))
                    deneme.add(getResources().getString(R.string.ogrenilen_dil_ispanyolca)+" 2");
                else if (files[i].getName().contains("-en-1"))
                    deneme.add(getResources().getString(R.string.ogrenilen_dil_ingilizce)+" 1");
                else if (files[i].getName().contains("-en-2"))
                    deneme.add(getResources().getString(R.string.ogrenilen_dil_ingilizce)+" 2");
                else if (files[i].getName().contains("-tr-1"))
                    deneme.add(getResources().getString(R.string.ogrenilen_dil_turkce)+" 1");
                else if (files[i].getName().contains("-tr-2"))
                    deneme.add(getResources().getString(R.string.ogrenilen_dil_turkce)+" 2");
                else if (files[i].getName().contains("-pt-1"))
                    deneme.add(getResources().getString(R.string.ogrenilen_dil_portekizce)+" 1");
                else if (files[i].getName().contains("-pt-2"))
                    deneme.add(getResources().getString(R.string.ogrenilen_dil_portekizce)+" 2");
                else if (files[i].getName().contains("-ru-1"))
                    deneme.add(getResources().getString(R.string.ogrenilen_dil_rusca)+" 1");
                else if (files[i].getName().contains("-ru-2"))
                    deneme.add(getResources().getString(R.string.ogrenilen_dil_rusca)+" 2");
                else if (files[i].getName().contains("-ko-1"))
                    deneme.add(getResources().getString(R.string.ogrenilen_dil_korece)+" 1");
                else if (files[i].getName().contains("-ko-2"))
                    deneme.add(getResources().getString(R.string.ogrenilen_dil_korece)+" 2");
            }
        }
        Collections.sort(deneme);
        final AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());

        ArrayAdapter adapter=new ArrayAdapter(getActivity(),android.R.layout.simple_list_item_1,
                android.R.id.text1,deneme);
        list.setAdapter(adapter);

        list.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                editor.putString("kullanilandil",dbadi.get(position));
                editor.putString("kullanilanpaket",deneme.get(position));
                editor.apply();
                datacekme();
                Toast.makeText(getActivity(),deneme.get(position),Toast.LENGTH_LONG).show();
            }
        });

        builder.setTitle("Dil seçimi");
        builder.setCancelable(true);

        builder.setView(view);
        builder.show();

    }

    public void datacekme(){
        sharedPreferences= getActivity().getSharedPreferences(getActivity().getPackageName(),Context.MODE_PRIVATE);
        String dbname=sharedPreferences.getString("kullanilandil", "default.db");
        String kullanilanPaket=sharedPreferences.getString("kullanilanpaket","");
        veritabanim=new Veritabanim(getActivity(),dbname);
        crud=new Crud();
        try {
            ArrayList<VeriCek> ogrenilenKelimeler= (ArrayList<VeriCek>) crud.sorguYurut(getActivity(),String.valueOf(tekrarSayisi),">=");

            ArrayList<VeriCek> tekrarlanacakKelimeler= (ArrayList<VeriCek>) crud.sorguYurut(getActivity(), String.valueOf(tekrarSayisi),"<");


            int ogrenilenKelimelerSize=ogrenilenKelimeler.size();
            int tekrarlanacakKelimelerSize=tekrarlanacakKelimeler.size();
            tekrarlanacakkelimetv2.setText(String.valueOf(getResources().getString(R.string.tekrarlanacak_kelimer)+tekrarlanacakKelimelerSize));
            ogrenilenkelimetv2.setText(String.valueOf(getResources().getString(R.string.ogrenilen_kelime)+ogrenilenKelimelerSize));
            long toplam= veritabanim.QueryNumEntries("language");
            toplamKelimeTv2.setText(getResources().getString(R.string.toplam_kelime)+String.valueOf(toplam));
            kullanılankelimepakedi_tv2.setText(getResources().getString(R.string.kullanilan_kelime_pakedi)+ "\n"+kullanilanPaket);
        }catch (Exception e) {

        }


    }
    private void init(){
        toplamKelimeTv2=v.findViewById(R.id.toplam_kelime_tv1);
        ogrenilenkelimetv2=v.findViewById(R.id.ogrenilen_kelime_tv1);
        kullanılankelimepakedi_tv2=v.findViewById(R.id.kullanılan_kelime_pakedi_tv1);
        tekrarlanacakkelimetv2=v.findViewById(R.id.tekrarlanacak_kelime_tv1);
        tekrarlanacakContsrain=v.findViewById(R.id.tekrarlanacak_kelimeler);
        ogrenilenConstrain=v.findViewById(R.id.ogrenilen_kelime);
        toplamKelimeContrain=v.findViewById(R.id.toplam_kelime);
        kullanilankelimepakedi=v.findViewById(R.id.kullanılan_kelime_pakedi);
        sharedPreferences=getActivity().getSharedPreferences(getActivity().getPackageName(), Context.MODE_PRIVATE);
        tekrarSayisi=sharedPreferences.getInt("tekrarsayisi",5);

        AdView mAdView = (AdView) v.findViewById(R.id.adViewIlerleme);
        AdRequest adRequest = new AdRequest.Builder().addTestDevice("5DE009055A7A5F306AB6397C2C4C0375").build();
        mAdView.loadAd(adRequest);
    }
}
