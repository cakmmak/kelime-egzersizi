package ezberleme.dilkelime.cakmak.kelimeegzersizi.fragmentler;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import ezberleme.dilkelime.cakmak.kelimeegzersizi.R;

public class FragmentKelimeOgrenme extends Fragment {

    public FragmentKelimeOgrenme() {
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v= inflater.inflate(R.layout.dialog, container, false);


        return v;
    }
}
