package ezberleme.dilkelime.cakmak.kelimeegzersizi.datebase;

/**
 * Created by cakmmak on 23.02.2018.
 */

public class VeriCek {

    String anaDil,hedefDil;
    int id,tekrarSayisi;

    public VeriCek() {

    }

    public VeriCek(String anaDil, String hedefDil, int id, int tekrarSayisi) {
        this.anaDil = anaDil;
        this.hedefDil = hedefDil;
        this.id = id;
        this.tekrarSayisi = tekrarSayisi;
    }

    public String getAnaDil() {
        return anaDil;
    }

    public void setAnaDil(String anaDil) {
        this.anaDil = anaDil;
    }

    public String getHedefDil() {
        return hedefDil;
    }

    public void setHedefDil(String hedefDil) {
        this.hedefDil = hedefDil;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getTekrarSayisi() {
        return tekrarSayisi;
    }

    public void setTekrarSayisi(int tekrarSayisi) {
        this.tekrarSayisi = tekrarSayisi;
    }
}
