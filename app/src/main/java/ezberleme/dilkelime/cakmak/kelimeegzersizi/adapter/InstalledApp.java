package ezberleme.dilkelime.cakmak.kelimeegzersizi.adapter;

import android.graphics.drawable.Drawable;

/**
 * Created by cakmmak on 4.01.2018.
 */

public class InstalledApp {
    private String appTitle;
    private String appPacke;
    private boolean checket;
    private Drawable appIcon;

    public boolean isChecket() {
        return checket;
    }

    public void setChecket(boolean checket) {
        this.checket = checket;
    }

    public String getAppPacke() {
        return appPacke;
    }

    public void setAppPacke(String appPacke) {
        this.appPacke = appPacke;
    }

    public String getAppTitle() {
        return appTitle;
    }

    public void setAppTitle(String appTitle) {
        this.appTitle = appTitle;
    }

    public Drawable getAppIcon() {
        return appIcon;
    }

    public void setAppIcon(Drawable appIcon) {
        this.appIcon = appIcon;
    }
}
