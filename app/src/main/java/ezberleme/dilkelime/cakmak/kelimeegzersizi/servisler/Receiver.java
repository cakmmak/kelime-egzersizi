package ezberleme.dilkelime.cakmak.kelimeegzersizi.servisler;

import android.annotation.SuppressLint;
import android.app.ActivityManager;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Build;


import com.rvalerio.fgchecker.AppChecker;


import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

public class Receiver extends BroadcastReceiver {
    Context context;
    @Override
    public void onReceive(Context context, Intent intent) {

        this.context=context;

        if ("android.intent.action.BOOT_COMPLETED".equals(intent.getAction())) {
            calistir();
        }
        calistir();

    }
      private void calistir(){

          AppChecker appChecker = new AppChecker();
          String packageName = appChecker.getForegroundApp(context);
          SharedPreferences sharedPrefs = context.getSharedPreferences(context.getPackageName(), Context.MODE_PRIVATE);
          SharedPreferences.Editor editor;
          editor=sharedPrefs.edit();
          boolean bl=sharedPrefs.getBoolean(packageName, false);
          boolean bb= isMyServiceRunning(FloatingViewService.class);
        /*
        //kilit ekranı
        PowerManager powerManager = (PowerManager) context.getSystemService(Context.POWER_SERVICE);
          assert powerManager != null;
          //boolean isScreenOn = powerManager.isScreenOn();

*/

          if (!bb){
              if (!bl&&isBackgroundRunning()){
                  Intent i = new Intent(context, FloatingViewService.class);
                  if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                      context.startForegroundService(i);
                  } else {
                      context.startService(i);
                  }
                  int toplam=sharedPrefs.getInt("toplam",0);
                  Date tarih = new Date();
                  @SuppressLint("SimpleDateFormat") SimpleDateFormat dakika = new SimpleDateFormat("dd-M-yyyy hh:mm:ss");
                  String ss=dakika.format(tarih);
                  editor.putString("tarih"+String.valueOf(toplam+1),ss);
                  editor.putInt("toplam",toplam+1);
                  editor.apply();

              }
          }
    }




      private boolean isMyServiceRunning(Class<?> serviceClass) {
          ActivityManager manager = (ActivityManager) context.getSystemService(Context.ACTIVITY_SERVICE);
          assert manager != null;
          for (ActivityManager.RunningServiceInfo service : manager.getRunningServices(Integer.MAX_VALUE)) {
              if (serviceClass.getName().equals(service.service.getClassName())) {
                  return true;
              }
          }
          return false;
      }

    private boolean isBackgroundRunning() {
        ActivityManager am = (ActivityManager) context.getSystemService(Context.ACTIVITY_SERVICE);
        assert am != null;
        List<ActivityManager.RunningAppProcessInfo> runningProcesses = am.getRunningAppProcesses();
        for (ActivityManager.RunningAppProcessInfo processInfo : runningProcesses) {
            if (processInfo.importance == ActivityManager.RunningAppProcessInfo.IMPORTANCE_FOREGROUND) {
                for (String activeProcess : processInfo.pkgList) {
                    if (activeProcess.equals(context.getPackageName())) {
                        //If your app is the process in foreground, then it's not in running in background
                        return false;
                    }
                }
            }
        }

        return true;
    }

}
