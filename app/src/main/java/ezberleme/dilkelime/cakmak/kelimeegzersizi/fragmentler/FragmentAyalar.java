package ezberleme.dilkelime.cakmak.kelimeegzersizi.fragmentler;

import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.app.AlertDialog;
import android.app.AppOpsManager;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.os.Bundle;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.constraint.ConstraintLayout;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.storage.FileDownloadTask;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;

import org.adw.library.widgets.discreteseekbar.DiscreteSeekBar;

import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import ezberleme.dilkelime.cakmak.kelimeegzersizi.adapter.AppPackeInfoAdapter;
import ezberleme.dilkelime.cakmak.kelimeegzersizi.adapter.DownloaddataBaseAdapter;
import ezberleme.dilkelime.cakmak.kelimeegzersizi.adapter.InstalledApp;
import ezberleme.dilkelime.cakmak.kelimeegzersizi.datebase.Crud;
import ezberleme.dilkelime.cakmak.kelimeegzersizi.datebase.Downloaddatabase;
import ezberleme.dilkelime.cakmak.kelimeegzersizi.servisler.FloatingViewService;
import ezberleme.dilkelime.cakmak.kelimeegzersizi.R;

public class FragmentAyalar extends Fragment {


    Switch toggleButtonTelaffuz;
    TextView tvTelaffuz,tvTekrarSayisi,tvSaatAraligi;
    SharedPreferences sharedPrefs;
    SharedPreferences.Editor editor ;
    DiscreteSeekBar discreteMultiple,discreteMultipleIki;
    Spinner sp;
    Crud crud;
    String spanadil,spanadilTam;
    Button btnindir;
    public FragmentAyalar() {

    }


    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }
    @Nullable
    @Override
    public View onCreateView(final LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        final View v= inflater.inflate(R.layout.fragment_ayarlar, container, false);

        sharedPrefs = getActivity().getSharedPreferences(getActivity().getPackageName(),Context.MODE_PRIVATE);
        editor = sharedPrefs.edit();
        int progresvarsayılan=sharedPrefs.getInt("tekrarsayisi",3);


        tvTelaffuz=v.findViewById(R.id.container_telaffuz_tv2);
        tvSaatAraligi=v.findViewById(R.id.saat_araligi_tv1);
        tvTekrarSayisi=v.findViewById(R.id.tekrar_tv1);
        sp=v.findViewById(R.id.spinner);
        btnindir=v.findViewById(R.id.btnindir);
        discreteMultiple=v.findViewById(R.id.discrete_multiple);
        discreteMultipleIki=v.findViewById(R.id.tekrar_sayisi_discrateseekbar);
        ConstraintLayout cs=v.findViewById(R.id.yasakli_uygulamalar);
        ConstraintLayout kl=v.findViewById(R.id.container_kelime_pakedi);

        initMultipleOfSeekBar();
        initMultipleOfSeekBarIki();

        sp.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                String ss=Locale.getDefault().getLanguage();

                switch (position) {
                    case 0:  spanadil = "de";break;
                    case 1:  spanadil = "ar";break;
                    case 2:  spanadil = "zh";break;
                    case 3:  spanadil = "hi";break;
                    case 4:  spanadil = "fr";break;
                    case 5:  spanadil = "ja";break;
                    case 6:  spanadil = "es";break;
                    case 7:  spanadil = "en";break;
                    case 8:  spanadil = "tr";break;
                    case 9:  spanadil = "pt";break;
                    case 10: spanadil = "ru";break;
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });




        btnindir.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                crud=new Crud();
                final ArrayList<Downloaddatabase> downloaddatabaseArrayList=crud.databaseDownload(getActivity(),spanadil);
                DownloaddataBaseAdapter adapter=new DownloaddataBaseAdapter(getActivity(),R.layout.download_list,R.id.download_list_tv,downloaddatabaseArrayList);
                final AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
                builder.setTitle("Dil Seçimi");
                builder.setCancelable(true);
                final View view = getActivity().getLayoutInflater().inflate(R.layout.dialog, null);
                final ListView list = (ListView) view.findViewById(R.id.list);
                list.setAdapter(adapter);
                builder.setView(view);
                builder.show();

                list.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                    @Override
                    public void onItemClick(AdapterView<?> parent, View view, final int position, long id) {
                        final String adi=downloaddatabaseArrayList.get(position).getDbadi();
                        FirebaseStorage storage = FirebaseStorage.getInstance();
                        StorageReference storageRef = storage.getReferenceFromUrl("gs://kelime-egzersizi.appspot.com/");
                        StorageReference pathReference = storageRef.child(adi);

                        File storagePath=new File("/data/data/"+getActivity().getPackageName()+"/databases/");
                        // Create direcorty if not exists

                        final File myFile = new File(storagePath,adi);

                        if(!myFile.exists()) {
                            pathReference.getFile(myFile).addOnSuccessListener(new OnSuccessListener<FileDownloadTask.TaskSnapshot>() {
                                @Override
                                public void onSuccess(FileDownloadTask.TaskSnapshot taskSnapshot) {
                                    Toast.makeText(getActivity(),"İndirme Tamamlandı",Toast.LENGTH_LONG).show();
                                    editor.putString("kullanilandil",adi);
                                    editor.putString("kullanilanpaket",downloaddatabaseArrayList.get(position).getAdi());
                                    editor.apply();
                                }
                            }).addOnFailureListener(new OnFailureListener() {
                                @Override
                                public void onFailure(@NonNull Exception exception) {
                                    // Handle any errors
                                    Toast.makeText(getActivity(),"İndirme Hatalı",Toast.LENGTH_LONG).show();

                                }
                            });
                        }else {
                            final AlertDialog.Builder builder=new AlertDialog.Builder(getActivity());
                            builder.setTitle(getResources().getString(R.string.kelime_pakedi));
                            builder.setMessage(getResources().getString(R.string.kelime_pakedi_dialog_message));
                            builder.setPositiveButton(getResources().getString(R.string.kelime_pakedi_dialog_positive), new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    editor.putString("kullanilandil",adi);
                                    editor.putString("kullanilanpaket",downloaddatabaseArrayList.get(position).getAdi());
                                    editor.commit();
                                }
                            });
                            builder.setNegativeButton(getResources().getString(R.string.kelime_pakedi_dialog_negative), new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                }
                            });

                            builder.show();

                        }
                    }
                });
            }
        });

        kl.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent i = new Intent(getActivity(), FloatingViewService.class);
                getActivity().startService(i);
                getActivity().finish();

            }
        });


        cs.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                
                if (!needsUsageStatsPermission())
                displayInstalledApps();
                else{
                    final AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
                    builder.setTitle(getResources().getString(R.string.izin_istegi_title));
                    builder.setCancelable(true);
                    builder.setNegativeButton(getResources().getString(R.string.izin_istegi_negative_button), new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                        }
                    });
                    builder.setMessage(getResources().getString(R.string.izin_istegi_mesaj));
                    builder.setPositiveButton(getResources().getString(R.string.izin_istegi_positive_button), new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            requestUsageStatsPermission();

                        }
                    });
                    builder.show();
                }
            }
        });

        boolean swicth=sharedPrefs.getBoolean("swicth",false);
        toggleButtonTelaffuz=v.findViewById(R.id.switch_telaffuz);
        toggleButtonTelaffuz.setChecked(swicth);
        toggleButtonTelaffuz.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (toggleButtonTelaffuz.isChecked()) {
                    tvTelaffuz.setText(R.string.kelime_telaffuzlari_kullaniliyor);
                    editor.putBoolean("swicth",true);
                } else {

                    tvTelaffuz.setText(R.string.kelime_telaffuzlari_kullanilmiyor);
                    editor.putBoolean("swicth",false);
                }
                editor.commit();
            }
        });

        return v;

    }

    public void displayInstalledApps() {

        final ArrayList<InstalledApp> apps_list = new ArrayList<InstalledApp>();

        Intent mainIntent = new Intent(Intent.ACTION_MAIN, null);
        mainIntent.addCategory(Intent.CATEGORY_LAUNCHER);
        PackageManager packManager = getActivity().getPackageManager();

        final List appsList = packManager.queryIntentActivities(mainIntent, 0);
        for (Object object : appsList) {
            ResolveInfo info = (ResolveInfo) object;
            Drawable icon = packManager.getApplicationIcon(info.activityInfo.applicationInfo);
            String appTitle = packManager.getApplicationLabel(info.activityInfo.applicationInfo).toString();
            String appPacke = info.activityInfo.packageName;
            InstalledApp app = new InstalledApp();
            app.setAppPacke(appPacke);
            app.setAppTitle(appTitle);
            app.setAppIcon(icon);
            apps_list.add(app);
        }

        AppPackeInfoAdapter adapter = new AppPackeInfoAdapter(getActivity(), R.layout.tek_satir, R.id.tv, apps_list);
        sharedPrefs = getActivity().getSharedPreferences(getActivity().getPackageName(), Context.MODE_PRIVATE);
        final SharedPreferences.Editor editor = sharedPrefs.edit();
        ArrayList<InstalledApp> apps;
        final AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setTitle("YASAKLI UYGULAMALAR");
        builder.setCancelable(true);
        final View view = (this).getLayoutInflater().inflate(R.layout.dialog, null);

        final ListView list = (ListView) view.findViewById(R.id.list);
        list.setAdapter(adapter);
        builder.setView(view);
        builder.show();


    }

    private void initMultipleOfSeekBar() {
        sharedPrefs= getActivity().getSharedPreferences(getActivity().getPackageName(),Context.MODE_PRIVATE);
        int progresvarsayılan=sharedPrefs.getInt("progresvarsayılan",3);

        discreteMultiple.setMin(3);
        discreteMultiple.setMax(15);
        tvSaatAraligi.setText(getResources().getString(R.string.saat_araligi)+": "+(progresvarsayılan*10));
        discreteMultiple.setProgress(progresvarsayılan);
        discreteMultiple.setNumericTransformer(new DiscreteSeekBar.NumericTransformer() {
            @Override
            public int transform(int value) {
                return value * 10;
            }
        });

        discreteMultiple.setOnProgressChangeListener(new DiscreteSeekBar.OnProgressChangeListener() {
            @SuppressLint("SetTextI18n")
            @Override
            public void onProgressChanged(DiscreteSeekBar seekBar, int value, boolean fromUser) {
                editor.putInt("saataraligi",value*10);
                editor.putInt("progresvarsayılan",value);
                int a=value*10;
                tvSaatAraligi.setText(getResources().getString(R.string.saat_araligi)+": "+a);
                editor.commit();
            }

            @Override
            public void onStartTrackingTouch(DiscreteSeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(DiscreteSeekBar seekBar) {

            }
        });
    }

    private void initMultipleOfSeekBarIki() {
        sharedPrefs= getActivity().getSharedPreferences(getActivity().getPackageName(),Context.MODE_PRIVATE);
        int progresvarsayılan=sharedPrefs.getInt("tekrarsayisi",3);

        discreteMultipleIki.setMin(3);
        discreteMultipleIki.setMax(10);
        tvTekrarSayisi.setText(getResources().getString(R.string.tekrar_sayisi)+": "+progresvarsayılan);
        discreteMultipleIki.setProgress(progresvarsayılan);
        discreteMultipleIki.setNumericTransformer(new DiscreteSeekBar.NumericTransformer() {
            @SuppressLint("SetTextI18n")
            @Override
            public int transform(int value) {
                return value;
            }
        });

        discreteMultipleIki.setOnProgressChangeListener(new DiscreteSeekBar.OnProgressChangeListener() {
            @Override
            public void onProgressChanged(DiscreteSeekBar seekBar, int value, boolean fromUser) {
                editor.putInt("tekrarsayisi",value);
                tvTekrarSayisi.setText(getResources().getString(R.string.tekrar_sayisi)+value);
                editor.commit();
            }

            @Override
            public void onStartTrackingTouch(DiscreteSeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(DiscreteSeekBar seekBar) {

            }
        });
    }


    private boolean needsUsageStatsPermission() {
        return postLollipop() && !hasUsageStatsPermission(getActivity());
    }

    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    private void requestUsageStatsPermission() {
        if(!hasUsageStatsPermission(getActivity())) {
            startActivity(new Intent(Settings.ACTION_USAGE_ACCESS_SETTINGS));
        }
    }

    private boolean postLollipop() {
        return android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP;
    }

    @TargetApi(Build.VERSION_CODES.KITKAT)
    private boolean hasUsageStatsPermission(Context context) {
        AppOpsManager appOps = (AppOpsManager) context.getSystemService(Context.APP_OPS_SERVICE);
        int mode = appOps.checkOpNoThrow("android:get_usage_stats",
                android.os.Process.myUid(), context.getPackageName());
        boolean granted = mode == AppOpsManager.MODE_ALLOWED;
        return granted;
    }

}
