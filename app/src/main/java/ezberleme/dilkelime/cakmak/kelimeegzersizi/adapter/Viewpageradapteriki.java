package ezberleme.dilkelime.cakmak.kelimeegzersizi.adapter;

import android.content.Context;
import android.support.constraint.ConstraintLayout;
import android.support.v4.view.PagerAdapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.List;

import ezberleme.dilkelime.cakmak.kelimeegzersizi.R;

public class Viewpageradapteriki extends PagerAdapter {

    private Context contex;
    List<Recmodel> recList;
    private LayoutInflater layoutInflater;

    public Viewpageradapteriki(Context context, List<Recmodel> recList){
        this.contex=context;
        this.recList=recList;
        layoutInflater= (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {
        return recList.size();
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view==object;
    }


    @Override
    public Object instantiateItem(ViewGroup container, int position) {
        View view=layoutInflater.inflate(R.layout.viewpager_tek_satir,container,false);

        ImageView image=view.findViewById(R.id.imageView9);
        TextView tv=view.findViewById(R.id.textView3);
        Recmodel gecici=recList.get(position);

        image.setImageResource(gecici.Resim);
        tv.setText(gecici.isim);

        container.addView(view);
        return view;
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        container.removeView((FrameLayout) object);
    }
}
