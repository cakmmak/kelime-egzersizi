package ezberleme.dilkelime.cakmak.kelimeegzersizi.fragmentler;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;

import java.util.ArrayList;
import java.util.List;

import ezberleme.dilkelime.cakmak.kelimeegzersizi.R;
import ezberleme.dilkelime.cakmak.kelimeegzersizi.adapter.OgrenilenArrayAdapter;
import ezberleme.dilkelime.cakmak.kelimeegzersizi.datebase.Crud;
import ezberleme.dilkelime.cakmak.kelimeegzersizi.datebase.VeriCek;


public class FragmentKelimeListesi extends Fragment {
    Crud crud;
    SharedPreferences sharedPreferences;

    public FragmentKelimeListesi() {
    }
    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v= inflater.inflate(R.layout.fragment_kelimeler, container, false);
        ListView listView=v.findViewById(R.id.kelimeler_list_view);

        crud=new Crud();
        sharedPreferences=getActivity().getSharedPreferences(getActivity().getPackageName(), Context.MODE_PRIVATE);
        int tekrarSayisi=sharedPreferences.getInt("tekrarsayisi",5);

        ArrayList<VeriCek> tekrarlanacakKelimeler= (ArrayList<VeriCek>) crud.sorguYurut(getActivity(),
                String.valueOf(tekrarSayisi),"<");
        OgrenilenArrayAdapter adapter=new OgrenilenArrayAdapter(getActivity(),R.layout.ogrenilen_layouth,R.id.ogrenilen_layauth_kelime,tekrarlanacakKelimeler);
        listView.setAdapter(adapter);

        AdView mAdView = (AdView) v.findViewById(R.id.adViewKelimeler);
        AdRequest adRequest = new AdRequest.Builder().addTestDevice("5DE009055A7A5F306AB6397C2C4C0375").build();
        mAdView.loadAd(adRequest);

        return v;
    }


}
