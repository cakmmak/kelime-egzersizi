package ezberleme.dilkelime.cakmak.kelimeegzersizi.adapter;

import android.content.Context;
import android.content.SharedPreferences;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.TextView;
import java.util.ArrayList;

import ezberleme.dilkelime.cakmak.kelimeegzersizi.R;


public class AppPackeInfoAdapter extends ArrayAdapter {

    private int vg;
    private ArrayList<InstalledApp> apps;
    private CheckBox ck;
    private Context context;
    private View itemView;

    public AppPackeInfoAdapter(Context context, int vg, int id, ArrayList<InstalledApp> apps) {

        super(context, vg, apps);
        this.context = context;
        this.vg = vg;
        this.apps = apps;
    }

    public View getView(final int position, View convertView, ViewGroup parent) {
        SharedPreferences sharedPrefs = context.getSharedPreferences(context.getPackageName(), Context.MODE_PRIVATE);
        final SharedPreferences.Editor editor = sharedPrefs.edit();

        itemView=convertView;
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        itemView = inflater.inflate(vg, parent, false);

        ImageView imageView = (ImageView) itemView.findViewById(R.id.imageView);

        imageView.setImageDrawable(apps.get(position).getAppIcon());

        TextView textAppName = (TextView) itemView.findViewById(R.id.tv);

        textAppName.setText(apps.get(position).getAppTitle());

        ck = (CheckBox) itemView.findViewById(R.id.ck);
        ck.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    editor.putBoolean(apps.get(position).getAppPacke(), isChecked);
                }
                else{
                        editor.remove(apps.get(position).getAppPacke());
                    }
                editor.apply();
            }
        });

        if (sharedPrefs.getBoolean(apps.get(position).getAppPacke(), false)) {
            ck.setChecked(true);
        }

        return itemView;

    }

}
