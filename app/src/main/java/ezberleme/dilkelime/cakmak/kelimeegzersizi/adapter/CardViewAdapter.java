package ezberleme.dilkelime.cakmak.kelimeegzersizi.adapter;

import android.content.Context;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.panshen.xps.popupcirclemenu.PopupButton;
import com.panshen.xps.popupcirclemenu.PopupCircleView;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

import ezberleme.dilkelime.cakmak.kelimeegzersizi.R;
import ezberleme.dilkelime.cakmak.kelimeegzersizi.fragmentler.FragmentOyun;

public class CardViewAdapter extends RecyclerView.Adapter<CardViewAdapter.MyViewHolder> {

    ArrayList<Recmodel> rc;
    Context mContex;
    int lastPosition=-1;
    private RecyclerViewItemClickListener recyclerViewItemClickListener;

    public CardViewAdapter(ArrayList<Recmodel> rcData, Context context) {
        this.rc=rcData;
        mContex=context;
    }

    @Override
    public CardViewAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        // Card_view layoutmuzu kullanarak yeni bir view inflate ettik
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.cardview_teksatir, parent, false);

        // Olusturdugumuz ViewHolder classına az önce olusturdugumuz view'ı verdik
        CardViewAdapter.MyViewHolder vh = new CardViewAdapter.MyViewHolder(v);


        v.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Toast.makeText(mContex,"Demeneşefskfms",Toast.LENGTH_LONG).show();
            }
        });

        return vh;
    }

    //item click e dokunma özelliği için kullandığımız metot
    public void setOnItemClickListener(RecyclerViewItemClickListener recyclerViewItemClickListener) {
        this.recyclerViewItemClickListener = recyclerViewItemClickListener;
    }
    @Override
    public void onBindViewHolder(CardViewAdapter.MyViewHolder holder, int position) {

        holder.position=position;
        Picasso.get().load(rc.get(position).Resim).fit().into(holder.image);
        //holder.image.setImageResource(rc.get(position).Resim);
        holder.tv.setText(rc.get(position).isim);
  /*      if(position >lastPosition) {

            Animation animation = AnimationUtils.loadAnimation(mContex,
                    R.anim.up_from_bottom);
            holder.itemView.startAnimation(animation);
            lastPosition = position;
        }*/

    }



    @Override
    public int getItemCount() {
        return rc.size();
    }







   public class MyViewHolder extends RecyclerView.ViewHolder{

        ImageView image;
        PopupCircleView popupCircleView;
        TextView tv;
        int position=0;

        public MyViewHolder(final View itemView) {
            super(itemView);
            image=itemView.findViewById(R.id.imageView8);
            tv=itemView.findViewById(R.id.cardviewtext);
            popupCircleView=itemView.findViewById(R.id.sample);

            popupCircleView.setmOnMenuEventListener(new PopupCircleView.OnMenuEventListener() {
                @Override
                public void onMenuToggle(PopupButton popupButton) {


                    switch(popupButton.getId()) {
                        case R.id.pb_favorite:
                            FragmentOyun fragmentOyun=new FragmentOyun();
                            android.support.v4.app.FragmentManager manager=((AppCompatActivity)mContex).getSupportFragmentManager();
                            android.support.v4.app.FragmentTransaction transaction=manager.beginTransaction();
                            Bundle bundle=new Bundle();
                            bundle.putInt("deneme",position+35);

                            fragmentOyun.setArguments(bundle);
                            transaction.add(R.id.main,fragmentOyun,"Ayarlar");

                            transaction.addToBackStack(null);
                            transaction.commit();
                            break;
                        case R.id.pb_like:
                            Toast.makeText(mContex,"Demeneşefskfms"+position,Toast.LENGTH_LONG).show();


                            break;
                        case R.id.pb_share:
                            Toast.makeText(mContex,"Demeneşefskfms"+position,Toast.LENGTH_LONG).show();

                            break;

                    }
                }
            });



            /*
             blurLayout=itemView.findViewById(R.id.sample);
            View hover = LayoutInflater.from(mContex).inflate(R.layout.sample, null);
            blurLayout.setHoverView(hover);
            blurLayout.getHoverStatus();

            blurLayout.addChildAppearAnimator(hover, R.id.imageView13, Techniques.FlipInX, 550, 0);
            blurLayout.addChildAppearAnimator(hover, R.id.imageView12, Techniques.FlipInX, 550, 250);*/

        }
    }

}
