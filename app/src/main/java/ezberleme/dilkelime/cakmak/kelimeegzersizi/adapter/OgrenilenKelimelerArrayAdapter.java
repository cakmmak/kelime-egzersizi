package ezberleme.dilkelime.cakmak.kelimeegzersizi.adapter;

import android.content.Context;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.CursorAdapter;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

import ezberleme.dilkelime.cakmak.kelimeegzersizi.R;
import ezberleme.dilkelime.cakmak.kelimeegzersizi.datebase.Crud;
import ezberleme.dilkelime.cakmak.kelimeegzersizi.datebase.VeriCek;
import ezberleme.dilkelime.cakmak.kelimeegzersizi.fragmentler.FragmentBasari;

public class OgrenilenKelimelerArrayAdapter extends ArrayAdapter {

    private int teksatir,textview;
    ArrayList<VeriCek> veriler;
    Crud crud=new Crud();
    Context context;
    View itemView;

    public OgrenilenKelimelerArrayAdapter(@NonNull Context context,int teksatir,int textview,ArrayList<VeriCek> veriler) {
        super(context,teksatir,veriler);
        this.context=context;
        this.teksatir=teksatir;
        this.textview=textview;
        this.veriler=veriler;
    }

    public View getView(final int position, final View convertView, final ViewGroup parent) {
        SharedPreferences sharedPrefs = context.getSharedPreferences(context.getPackageName(), Context.MODE_PRIVATE);
        final SharedPreferences.Editor editor = sharedPrefs.edit();

        itemView=convertView;
        if (itemView==null) {
            LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            itemView = inflater.inflate(teksatir, parent, false);

        }

        final TextView ogrenilenlayauthanlami = (TextView) itemView.findViewById(R.id.ogrenilen_kelimeler_layauth_anlami);
        TextView ogrenilenlayauthkelime = (TextView) itemView.findViewById(R.id.ogrenilen_kelimeler_layauth_kelime);
        TextView ogrenilenlayauthkullanilan = (TextView) itemView.findViewById(R.id.ogrenilen_kelimeler_layauth_kullanilan);
        ImageButton imgButton=itemView.findViewById(R.id.imageButton);

        ogrenilenlayauthanlami.setText(veriler.get(position).getAnaDil());
        ogrenilenlayauthkelime.setText(veriler.get(position).getHedefDil());
        ogrenilenlayauthkullanilan.setText(String.valueOf(veriler.get(position).getTekrarSayisi()+" defa tekrarlandı."));

        imgButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int id=veriler.get(position).getId();
                crud.veriGuncelle(context,0,id);
                veriler.remove(position);
                notifyDataSetChanged();
            }
        });
        return itemView;

    }

}


