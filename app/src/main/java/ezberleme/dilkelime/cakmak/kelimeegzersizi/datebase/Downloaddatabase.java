package ezberleme.dilkelime.cakmak.kelimeegzersizi.datebase;

/**
 * Created by cakmmak on 13.04.2018.
 */

public class Downloaddatabase {
    private String link;
    private String adi;
    private String dbadi;

    public Downloaddatabase() {
    }

    public Downloaddatabase(String link, String adi, String dbadi) {
        this.link = link;
        this.adi = adi;
        this.dbadi = dbadi;
    }

    public String getDbadi() {
        return dbadi;
    }

    public void setDbadi(String dbadi) {
        this.dbadi = dbadi;
    }

    public String getLink() {
        return link;
    }

    public void setLink(String link) {
        this.link = link;
    }

    public String getAdi() {
        return adi;
    }

    public void setAdi(String adi) {
        this.adi = adi;
    }
}
