package ezberleme.dilkelime.cakmak.kelimeegzersizi.adapter;

import android.content.Context;
import android.content.SharedPreferences;
import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;

import ezberleme.dilkelime.cakmak.kelimeegzersizi.R;
import ezberleme.dilkelime.cakmak.kelimeegzersizi.datebase.Crud;
import ezberleme.dilkelime.cakmak.kelimeegzersizi.datebase.VeriCek;

public class ListviewDeneme extends ArrayAdapter {


    private int teksatir,textview;
    private ArrayList<Recmodel> veriler;
    private Context context;
    private View itemView;

    public ListviewDeneme(@NonNull Context context, int teksatir, int textview, ArrayList<Recmodel> veriler) {
        super(context,teksatir,veriler);
        this.context=context;
        this.teksatir=teksatir;
        this.textview=textview;
        this.veriler=veriler;
    }

    public View getView(final int position, final View convertView, final ViewGroup parent) {


            LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            itemView = inflater.inflate(teksatir, parent, false);

        ImageView image=itemView.findViewById(R.id.imageView7);
        TextView tv=itemView.findViewById(R.id.textView2);

        Picasso.get().load(veriler.get(position).Resim).fit().into(image);
        tv.setText(veriler.get(position).isim);
        tv.setSelected(true);




        return itemView;

    }

}
