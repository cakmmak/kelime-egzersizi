package ezberleme.dilkelime.cakmak.kelimeegzersizi.servisler;

import android.app.Notification;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.PixelFormat;
import android.os.IBinder;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import java.io.IOException;
import java.util.Random;

import ezberleme.dilkelime.cakmak.kelimeegzersizi.R;
import ezberleme.dilkelime.cakmak.kelimeegzersizi.datebase.Crud;
import ezberleme.dilkelime.cakmak.kelimeegzersizi.datebase.VeriCek;
import ezberleme.dilkelime.cakmak.kelimeegzersizi.datebase.Veritabanim;

/**
 * Created by cakmmak on 13.03.2018.
 */

public class FloatingViewServiceIki extends Service {

    private WindowManager mWindowManager;
    private View mFloatingView;
    String anlami0, kelime;
    int kullanilan;
    Crud crud;
    Veritabanim veri;
    VeriCek veriCek;
    SharedPreferences sharedPreferences;
    public FloatingViewServiceIki() {
    }

    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {

        mFloatingView = LayoutInflater.from(this).inflate(R.layout.floating_window_iki, null);
        startForeground(1,new Notification());

        //Ekrana, view elementi eklemek için ayar yapıyorz
        final WindowManager.LayoutParams params = new WindowManager.LayoutParams(
                WindowManager.LayoutParams.WRAP_CONTENT,
                WindowManager.LayoutParams.WRAP_CONTENT,
                WindowManager.LayoutParams.TYPE_APPLICATION_OVERLAY,
                WindowManager.LayoutParams.FLAG_NOT_TOUCH_MODAL,
                PixelFormat.TRANSLUCENT);
        params.gravity = Gravity.CLIP_HORIZONTAL;        //Başlangıçta görünüm, sol üst köşeye eklenecek
        params.x = 0;
        params.y = 100;
        //Ekrana, layout icinde barınan view elementi ekliyoruz
        mWindowManager = (WindowManager) getSystemService(WINDOW_SERVICE);
        mWindowManager.addView(mFloatingView, params);

        sharedPreferences= getApplicationContext().getSharedPreferences(getApplicationContext().getPackageName(), Context.MODE_PRIVATE);

        String dbname=sharedPreferences.getString("kullanilandil","en-tr.db");


        veri = new Veritabanim(getApplicationContext(),dbname);
        crud = new Crud();
        try {
            veri.createDataBase();
        } catch (IOException e) {
            e.printStackTrace();
        }

        veriCek = crud.tekveriGetir(getApplicationContext());
        anlami0 = veriCek.getAnaDil();
        kelime = veriCek.getHedefDil();
        kullanilan = veriCek.getTekrarSayisi();

        final TextView tv=mFloatingView.findViewById(R.id.floating_window2_texview);
        final TextView tv2=mFloatingView.findViewById(R.id.floating_window2_texview2);
        final ImageButton deleteButton=mFloatingView.findViewById(R.id.deletebutton);
        tv.setText(kelime);

         final String[] karıstır=new String[anlami0.length()];

        for (int i=0;i<=anlami0.length()-1;i++){
            karıstır[i]= String.valueOf(anlami0.charAt(i));
        }

        for (int j=0;j<=karıstır.length-1;j++){
            int tg= (int) (Math.random()*karıstır.length);
            String k=karıstır[tg];
            karıstır[tg]=karıstır[j];
            karıstır[j]=k;
            //tv2.setText((tv2.getText().toString())+" _");
        }

       LinearLayout llOrtaAlan = (LinearLayout) mFloatingView.findViewById(R.id.floating_window2_linearlayauth);
        llOrtaAlan.setOrientation(LinearLayout.HORIZONTAL);

        for (int i=0;i<=karıstır.length-1;i++) {
            Button btnOrnek = new Button(getApplicationContext());
            LinearLayout.LayoutParams btnOrnek1Par = new LinearLayout.LayoutParams(100,100);
            // Hazırlanan değerler atanıyor
            btnOrnek.setLayoutParams(btnOrnek1Par);
            // Button kontrol'ünün text özelliği atanıyor
            btnOrnek.setText(karıstır[i]);
            btnOrnek.setBackgroundColor(getResources().getColor(R.color.beyazz));
            btnOrnek.setTextColor(getResources().getColor(R.color.anarenk));
            final String ss=btnOrnek.getText().toString();
            llOrtaAlan.addView(btnOrnek,btnOrnek1Par);
            btnOrnek.setOnClickListener(new View.OnClickListener() {
                public void onClick(View v) {
                   tv2.setText(tv2.getText().toString()+ss);

                   if (anlami0.equalsIgnoreCase(tv2.getText().toString())){
                       Toast.makeText(getApplicationContext(),"tebrikler",Toast.LENGTH_LONG).show();
                       stopSelf();
                   }
                }
            });


        }

        deleteButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int length = tv2.getText().length();
                if (length > 0) {
                   String newtext= tv2.getText().toString().substring(0,length - 1);
                   tv2.setText(newtext);
                }
            }
        });

        return START_NOT_STICKY;
    }


    @Override
    public void onDestroy() {
        super.onDestroy();
        if (mFloatingView != null) mWindowManager.removeView(mFloatingView);
    }
}