package ezberleme.dilkelime.cakmak.kelimeegzersizi.fragmentler;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;

import ezberleme.dilkelime.cakmak.kelimeegzersizi.R;
import ezberleme.dilkelime.cakmak.kelimeegzersizi.adapter.CardViewAdapter;
import ezberleme.dilkelime.cakmak.kelimeegzersizi.adapter.MyAdapter;
import ezberleme.dilkelime.cakmak.kelimeegzersizi.adapter.Recmodel;
import ezberleme.dilkelime.cakmak.kelimeegzersizi.adapter.Viewpageradapteriki;
import ezberleme.dilkelime.cakmak.kelimeegzersizi.datebase.Crud;
import ezberleme.dilkelime.cakmak.kelimeegzersizi.datebase.VeriCek;

public class FragmentOyun extends Fragment {

    Crud crud;

    public FragmentOyun() {
    }
    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v= inflater.inflate(R.layout.ogrenme_layouth, container, false);

        int resimler[]={R.drawable.insanlar,R.drawable.gorunum,R.drawable.saglik,R.drawable.ev,
                R.drawable.hizmetler,R.drawable.alisveris,R.drawable.yiyecekler,R.drawable.fesfood,R.drawable.egitim,
                R.drawable.ishayati,R.drawable.ulasim,R.drawable.spor,R.drawable.boszamanlar,R.drawable.dogalcevre,
                R.drawable.bilgilenme};


        final ArrayList<Recmodel> rc=new ArrayList<Recmodel>();
        ArrayList<VeriCek> veriCeks=new ArrayList<>();

        int sayi=getArguments().getInt("deneme");
        crud=new Crud();
        veriCeks=crud.recVericek(getActivity(),"default.db",sayi);

        for (int i=0;i<=resimler.length-1;i++){

            Recmodel rec=new Recmodel();
            rec.setIsim(veriCeks.get(i).getAnaDil());
            rec.setResim(resimler[i]);
            rc.add(rec);

        }


         ViewPager viewPager=(ViewPager) v.findViewById(R.id.viewPager);
        Viewpageradapteriki viewpageradapteriki=new Viewpageradapteriki(getActivity(),rc);

        viewPager.setAdapter(viewpageradapteriki);

        return v;
    }


}
